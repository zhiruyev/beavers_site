function moveBeaverNames() {
    const beaverNameElements = document.querySelectorAll('.beaver-name');

    beaverNameElements.forEach(function (element) {
        const randomAngle = Math.random() * 2 * Math.PI; // Random angle in radians
        const radius = Math.min(window.innerWidth, window.innerHeight) / 4; // Max radius

        const randomTop = window.innerHeight / 2 + radius * Math.sin(randomAngle) - element.clientHeight / 2;
        const randomLeft = window.innerWidth / 2 + radius * Math.cos(randomAngle) - element.clientWidth / 2;

        // Apply the new positions with a smooth transition
        element.style.transform = `translate(${randomLeft}px, ${randomTop}px)`;
    });
}

moveBeaverNames();

document.addEventListener('DOMContentLoaded', function () {
    setInterval(moveBeaverNames, 10000);
});
